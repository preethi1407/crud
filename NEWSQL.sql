-- creating a new database 
create database preethi;

-- using the created database 
use preethi;

-- list of databases
show databases;

-- creating a new table 
create table employee(
employee_name varchar(10),
employee_id int,
email varchar(30),
contact int);

-- displaying all tables
show tables;

-- dropping tables 
drop table employee;

-- obtaining the schema of the table 
describe employee_table;

-- inserting values into the created table 
insert into employee(employee_name,employee_id,email,contact)
values ('preethi',10965,'pkommabathula@inominds.com',833192);

-- obtaining all the information of the table
select * from employee;

-- obtaining the details of the particular field in a table
select employee_name from employee;

-- updating a particular column in a table
update employee
set employee_name='preethi k'
where employee_id=10965;

-- adding a new column to an existing table
alter table employee
add dob datetime;
 
-- dropping a column in an existing table
alter table employee
drop column dob;

-- drop database
drop database preethi;




















w    

modifying column datatype in an existing table
alter table employee_table
modify column employee_id varchar(20);

-- changing the name of the table 
alter table employee rename to employee_table;

