import mysql.connector
my_database=input("enter the database you want to use ")
class crud_operations:
    def validate(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000"
        )

        mycursor = mydb.cursor()

        mycursor.execute("show databases")

        for x in mycursor:
            x = str(x[0])
            if x==my_database:
                print("database already exists")
                print("using the database",my_database)
                mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                password="Preethi2000",
                database=my_database
                )
                return my_database
        else:
            print("creating a new database")
            mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="Preethi2000"
            )


            mycursor = mydb.cursor()

            mycursor.execute(f"CREATE DATABASE {my_database}")
            print("database created successfully")

            return my_database
    def show_databases(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate()    
        )

        mycursor = mydb.cursor()
        mycursor.execute("show databases")

        for x in mycursor:
            print(x)


    def create_table(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate()    
        )
        table_name=input("enter the table name you want to create")
        mycursor = mydb.cursor()

        mycursor.execute(f"CREATE TABLE {table_name} (employee_name varchar(10),employee_id int,email varchar(30),contact int)")
        print(f"table {table_name} created successfully ")

    def show_tables(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate()    
        )

        mycursor = mydb.cursor()

        mycursor.execute("show tables")

        for x in mycursor:
            print(x)

    def insert_into_table(self):
        
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate()    
        )
        emp_name=input("enter the employee name you want to insert")
        emp_id=input("enter the employee id")
        email=input("enter the employee email address")
        contact=input("enter the employee contact number")
        table_name=input("enter the table name you want to insert values into")
        mycursor = mydb.cursor()
        sql = f"INSERT INTO {table_name}  (employee_name,employee_id,email,contact) VALUES (%s, %s,%s,%s)"
        val = (emp_name,emp_id,email,contact)
        mycursor.execute(sql, val)

        mydb.commit()

        print(mycursor.rowcount, "record inserted.")


    def display_data(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        mycursor = mydb.cursor()
        table_name=input("enter the table name you want to view contents")
        mycursor.execute(f"SELECT * FROM {table_name}")

        myresult = mycursor.fetchall()

        for x in myresult:
            print(x)

    def table_schema(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate()) 
        table_name=input('enter the table for which you want to obtain the schema')
        mycursor = mydb.cursor()
        mycursor.execute(f"describe {table_name}")
        for y in mycursor:
            print(y)

    def select_particular_record(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        table_name=input("enter the table name ")
        emp_id=input("enter the employee id for which you want to obtain the details")

        mycursor = mydb.cursor()
        mycursor.execute(f"SELECT employee_id FROM {table_name}")
        myresult = mycursor.fetchall()
        for x in myresult:
            x = str(x[0])
            if x==emp_id:
                mycursor.execute(f"SELECT * FROM {table_name} WHERE employee_id={emp_id}")
                myresult = mycursor.fetchall()
                for x in myresult:
                    print(x)
            else:
                print("invalid id")

    def rename_table(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        table_name=input("enter the table name you want to rename")
        new_name=input("enter the new table name")
        mycursor = mydb.cursor()
        mycursor.execute(f"ALTER table {table_name} RENAME  To {new_name}")
        print('table name changed')

    def add_column(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        table_name=input("enter the table name")
        column_name=input("enter the new column name")
        column_type=input("enter the new column type")
        
        mycursor = mydb.cursor()
        mycursor.execute(f"ALTER table {table_name} ADD {column_name} {column_type}")
        print("column added successfully")

    def update_record(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        table_name=input("enter the table name")
        emp_id=(input("enter the employee id you want to modify"))
        
        column=input("enter the column you want to modify")
        new_value=input("enter the new value ")
        mycursor = mydb.cursor()
        mycursor.execute(f"SELECT employee_id FROM {table_name} ")
        myresult = mycursor
        for x in myresult:
            print(x)
            x = str(x[0])
            print(type(x),type(emp_id),x,emp_id)
            if x==emp_id:
                mycursor = mydb.cursor()
                # mycursor.execute(f"UPDATE {table_name} set {column}={new_value} WHERE employee_id={emp_id}")
                mycursor.execute("update %s set %s=%s where employee_id=%s"%(table_name,column,new_value,emp_id))
                print(mycursor.rowcount, "record(s) affected")
            else:
                print("invalid id")

    def del_database(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        mycursor = mydb.cursor()
        mycursor.execute(f"DROP database IF EXISTS {my_database}")
        print("database deleted successfully")

    def drop_table(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        table=input("enter the name of the table you want to drop")
        mycursor = mydb.cursor()
        mycursor.execute(f"DROP TABLE IF EXISTS {table}")
        print("table dropped successfully")

    def del_record(self):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Preethi2000",
        database=self.validate() )
        mycursor = mydb.cursor()

        table_name=input("enter the table name")
        emp_id=input("enter the employee id")
        sql = f"DELETE FROM {table_name} WHERE employee_id ={emp_id}"
        mycursor.execute(sql)

        mydb.commit()

        print(mycursor.rowcount, "record(s) deleted")

    def Employee_menu(self):
        print("")
        print("___________E M P L O Y E E  D A T A B A S E___________")
        print("1 -Shows all the databases")
        print("2 -creating a new table")
        print("3 -showing all the tables in a database")
        print("4 -inserting values into a table")
        print("5 -displaying all the contents in a table")
        print("6 -description of a table(schema)")
        print("7 -view the details of a particular record")
        print("8 -rename table")
        print("9 -adding a new column to an existing table")
        print("10 -updating record")
        print("11 -delete database)")
        print("12 -drop table")
        print("13 -delete record")
        user_input=input("Choose option 1-13 or '14' to exit: ")

        while user_input !="14":

            if user_input =="1":
                self.show_databases()
                break

            if user_input =="2":
                self.create_table()
                break

            if user_input =="3":
                self.show_tables()
                break

            if user_input =="4":
                self.insert_into_table()
                break

            if user_input =="5":
                self.display_data()
                break

            if user_input =="6":
                self.table_schema()
                break

            if user_input =="7":
                self.select_particular_record()
                break

            if user_input =="8":
                self.rename_table()
                break

            if user_input =="9":
                self.add_column()
                break

            if user_input =="10":
                self.update_record()
                break

            if user_input =="11":
                self.del_database()
                break

            if user_input =="12":
                self.drop_table()
                break

            if user_input =="13":
                self.del_record()
                break


